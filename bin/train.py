import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(__file__)))

from src.train import train

if __name__ == '__main__':
    train()
