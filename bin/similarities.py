import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(__file__)))

from src.train import save_similarities

if __name__ == '__main__':
    save_similarities()
