from sklearn import feature_extraction
from sklearn import feature_selection
from sklearn import svm
from sklearn import preprocessing

import src.graph
import src.stemmer

GRID_SEARCH = {
    'graph': {
        'steps': [
            ('tokenizer', src.graph.TokenizeTransformer()),
            ('vectorizer', src.graph.WordGraphSimilarity(window_size=5)),
        ]
    },
    'graph_svc': {
        'params': [{
            'clf__C': [0.1, 1, 10],
            'clf__kernel': ['rbf'],
            'vectorizer__window_size': [2, 3, 5],
            'vectorizer__self_edges': [False, True],
            'vectorizer__weighted': [False, True],
            'vectorizer__weighted_classes': [False, True],
            'vectorizer__scaler': [None, preprocessing.scale, preprocessing.minmax_scale],
            'tokenizer__preprocessor': [None, src.stemmer.stem_document]
        }],
        'steps': [
            ('tokenizer', src.graph.TokenizeTransformer()),
            ('vectorizer', src.graph.WordGraphSimilarity(0)),
            ('clf', svm.SVC(max_iter=5000))
        ]
    },
    'tfidf_lin_svc': {
        'params': [{
            'clf__C': [0.1, 1, 10, 100],
            'anova__k': [100, 500, 1000],
            'vectorizer__ngram_range': [(1, 1), (1, 2), (2, 2), (3, 3)]
        }],
        'steps': [
            ('vectorizer', feature_extraction.text.TfidfVectorizer()),
            ('anova', feature_selection.SelectKBest()),
            ('clf', svm.LinearSVC(max_iter=1500))
        ]
    },
    'count_vectorizer_lin_svc': {
        'params': [{
            'clf__C': [0.1, 1, 10, 100],
            'anova__k': [100, 500, 1000],
            'vectorizer__ngram_range': [(1, 1), (1, 2), (2, 2), (3, 3)]
        }],
        'steps': [
            ('vectorizer', feature_extraction.text.CountVectorizer()),
            ('anova', feature_selection.SelectKBest()),
            ('clf', svm.LinearSVC(max_iter=1500))
        ]
    }
}
