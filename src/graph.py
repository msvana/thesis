import numpy
from sklearn import base
from sklearn import feature_extraction


class Graph:

    def __init__(self, weighted: bool = True):
        self._graph = {}
        self._edge_cnt = None
        self._weighted = weighted

    def get_edge_count(self) -> int:
        if self._edge_cnt is None:
            total_count = 0
            for node, neighbours in self._graph.items():
                total_count += sum(neighbours.values())
            self._edge_cnt = total_count
        return self._edge_cnt

    def add_edge(self, edge: tuple):
        if edge[0] not in self._graph:
            self._graph[edge[0]] = dict()
        if edge[1] not in self._graph[edge[0]]:
            self._graph[edge[0]][edge[1]] = 0 if self._weighted else 1
        if self._weighted:
            self._graph[edge[0]][edge[1]] += 1

    @property
    def structure(self):
        return self._graph

    def get_common_edge_count(self, other: 'Graph'):
        total_count = 0
        for node, neigbours in self._graph.items():
            if node in other.structure:
                for neigbour_name, weight in neigbours.items():
                    if neigbour_name in other.structure[node]:
                        other_weight = other.structure[node][neigbour_name]
                        total_count += min(weight, other_weight)
        return total_count


class WordGraphSimilarity(base.TransformerMixin):
    """
    Creates a new set of features describing a text document in classification tasks
    using word-graph method.
    """

    def __init__(self,
            window_size: int, skip_size: int = 0.0, self_edges: bool = False,
            weighted: bool = True, weighted_classes: bool = True, scaler=None):
        self._class_graphs = {}
        self._window_size = window_size
        self._skip_size = skip_size
        self._fitted_y = None
        self._self_edges = self_edges
        self._weighted = weighted
        self._weighted_classes = weighted_classes
        self._scaler = scaler

    def set_params(self, **params):
        for param, val in params.items():
            self.__setattr__('_%s' % param, val)

    def get_params(self, **_):
        return {
            'window_size': self._window_size,
            'skip_size': self._skip_size,
            'self_edges': self._self_edges,
            'weighted': self._weighted,
            'weighted_classes': self._weighted_classes,
            'scaler': self._scaler
        }

    def fit(self, X, y) -> 'WordGraphSimilarity':
        self._class_graphs = {}
        self._fitted_y = y

        """
        Create a graph for each class in the dataset as a merge of all document graphs

        :param X: Array-like object containing text documents
        :param y: Array-like object containing classes for text documents
        """
        for i in range(len(X)):
            graph_name = y[i]
            if graph_name not in self._class_graphs:
                self._class_graphs[graph_name] = Graph(weighted=self._weighted)
            for edge in self._iter_edges(X[i]):
                self._class_graphs[graph_name].add_edge(edge)
        return self

    def transform(self, X: numpy.ndarray) -> numpy.ndarray:
        X_transformed = []

        for i in range(0, X.shape[0]):
            doc = X[i]
            similarities = []
            document_graph = Graph(weighted=self._weighted)

            for edge in self._iter_edges(doc):
                document_graph.add_edge(edge)

            doc_edges = document_graph.get_edge_count()

            for class_graph in self._class_graphs.values():
                class_edges = class_graph.get_edge_count()
                common_edges = document_graph.get_common_edge_count(class_graph)
                similarity = (common_edges / min(doc_edges, class_edges))
                if self._weighted_classes:
                    similarity /= numpy.log(class_edges)
                similarities.append(similarity)

            X_transformed.append(similarities)

        if self._scaler:
            X_transformed = self._scaler(X_transformed, axis=1)
        else:
            X_transformed = numpy.array(X_transformed)
        return X_transformed

    def _iter_edges(self, doc):
        for window_start in range(len(doc)):
            try:
                range_start = 0 if self._self_edges else 1
                for i in range(range_start, self._window_size):
                    yield doc[window_start], doc[window_start + i]
            except IndexError:
                continue


class TokenizeTransformer(base.TransformerMixin, feature_extraction.text.VectorizerMixin):
    token_pattern = r'(?u)\b\w\w+\b'
    tokenizer = None

    def __init__(self, preprocessor=None):
        self._preprocessor = preprocessor

    def set_params(self, **params):
        for param, val in params.items():
            self.__setattr__('_%s' % param, val)

    def get_params(self, **_) -> dict:
        return {'preprocessor': self._preprocessor}

    def fit(self, X, y):
        return self

    def transform(self, X: numpy.ndarray) -> numpy.ndarray:
        X_transformed = []
        tokenize = self.build_tokenizer()
        for doc in X:
            if self._preprocessor:
                doc = self._preprocessor(doc)
            tokens = [t.lower() for t in tokenize(doc)]
            X_transformed.append(tokens)
        return numpy.array(X_transformed)
