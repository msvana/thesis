FROM continuumio/miniconda3

RUN conda install \
    numpy \
    pandas \
    scikit-learn \
    nltk

RUN pip install neo4j-driver
RUN python -c "import nltk; nltk.download('punkt')"

ADD . /app
WORKDIR /app
VOLUME /data

ENTRYPOINT ["python", "-u", "train.py"]